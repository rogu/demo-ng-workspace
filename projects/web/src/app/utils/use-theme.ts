import { computed } from '@angular/core';
import { useLocalStorage } from './use-localstorage';

export function useTheme() {
    const storage = useLocalStorage('theme');
    const isDark = computed(() => storage.value() === 'dark' ? true : false);
    const setTheme = (value: boolean) => storage.value.set(value ? 'dark' : 'light');
    return { isDark, setTheme };
}
