import { Routes } from '@angular/router';
import { AuthComponent } from './features/auth/auth.component';
import { InitComponent } from './core/components/init/init.component';

export const routes: Routes = [
  {
    path: 'pages',
    children: [
      {
        path: 'items',
        loadComponent: () =>
          import('./features/items/items.component').then((c) => c.ItemsComponent),
      },
      {
        path: 'contact',
        loadComponent: () =>
          import('./features/contact-form/contact-form.component').then(
            (c) => c.ContactFormComponent
          ),
        data: {
          url: 'https://api.debugger.pl/utils/contact-form',
        },
      },
      {
        path: 'auth',
        component: AuthComponent,
        children: [
          {
            path: 'login',
            loadComponent: () =>
              import('./features/auth/login-form.component.ts.component').then(
                (c) => c.LoginFormComponentTsComponent
              ),
          },
          {
            path: 'register',
            loadComponent: () =>
              import('projects/web/src/app/features/auth/register-form-custom.component').then(
                (c) => c.RegisterFormCustomComponent
              ),
          },
        ],
      },
    ],
  },
  {
    path: '**',
    component: InitComponent,
  },
];
