import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, inject } from '@angular/core';
import { Observable, map } from 'rxjs';
import { Api } from '../../utils/api';
import { FormGeneratorComponent, FieldGeneratorDirective, FieldConfig, FormValue } from 'ng-ui-lib';

@Component({
  selector: 'app-register-form',
  standalone: true,
  imports: [FormGeneratorComponent, CommonModule, FieldGeneratorDirective],
  template: `
      <div>
        <h3>Register | Form Generator - automatyczne ułożenie kontrolek</h3>
        <lib-form-generator (action)="onSubmit($event)" [formConfig]="formConfig$ | async"></lib-form-generator>
        @for (error of errors; track error) {
          <div>{{ error | json }}</div>
        }
      </div>
    `
})
export class RegisterFormComponent implements OnInit {
  http = inject(HttpClient);
  formConfig$!: Observable<FieldConfig[]>;
  errors: any;
  ngOnInit(): void {
    this.formConfig$ = this.http.get(Api.FORM_CONFIG_END_POINT).pipe(map((resp: any) => resp.data));
  }
  onSubmit(ev: FormValue) {
    this.errors = ev.errors;
    console.log(ev.payload);
  }
}
