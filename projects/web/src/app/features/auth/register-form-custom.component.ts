import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, inject } from '@angular/core';
import { Observable, map } from 'rxjs';
import { Api } from '../../utils/api';
import { FormGeneratorComponent, FieldGeneratorDirective, FieldConfig, FormValue } from 'ng-ui-lib';

@Component({
  selector: 'app-register-form-custom',
  standalone: true,
  imports: [FormGeneratorComponent, CommonModule, FieldGeneratorDirective],
  template: `
    <div>
      <h3>Register | Form Generator - nietypowe ułożenie kontrolek</h3>
      @if (formConfig$ | async; as config) {
        <lib-form-generator (action)="onSubmit($event)" [formConfig]="config">
          <ng-template>
            <div class="flex gap-6">
              @for (row1 of [0,1]; track $index) {
                <ng-container [libFieldGenerator]="config[row1]"></ng-container>
              }
            </div>
            <div class="flex gap-6">
              @for (row2 of [2,3,4]; track $index) {
                <ng-container [libFieldGenerator]="config[row2]"></ng-container>
              }
            </div>
            <div class="flex gap-6">
            @for (row3 of [5,6]; track $index) {
              <ng-container [libFieldGenerator]="config[row3]"></ng-container>
            }
            </div>
            <ng-container [libFieldGenerator]="config[7]"></ng-container>
            <ng-container [libFieldGenerator]="config[8]"></ng-container>
          </ng-template>
        </lib-form-generator>
      }
      @for (error of errors; track error) {
        <div>{{ error | json }}</div>
      }
    </div>
    `
})
export class RegisterFormCustomComponent implements OnInit {
  onSubmit($event: FormValue) {
    this.errors = $event.errors;
    console.log($event.payload);
  }
  http = inject(HttpClient);
  formConfig$!: Observable<FieldConfig[]>;
  errors: any;
  ngOnInit(): void {
    this.formConfig$ = this.http.get(Api.FORM_CONFIG_END_POINT).pipe(map((resp: any) => resp.data));
  }
}
