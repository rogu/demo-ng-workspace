import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatTabNavPanel, MatTabsModule } from '@angular/material/tabs';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-auth',
  standalone: true,
  imports: [RouterModule, CommonModule, MatTabsModule, MatTabNavPanel],
  templateUrl: './auth.component.html',
  styles: ``
})
export class AuthComponent {
  tabs: any[] = [
    {
      label: 'LOGIN',
      route: '/pages/auth/login',
    },
    {
      label: 'REGISTER',
      route: '/pages/auth/register',
    }
  ];
}
