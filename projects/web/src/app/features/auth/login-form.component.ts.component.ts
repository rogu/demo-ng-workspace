import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FieldConfig, FieldTypes, FormGeneratorComponent, createFieldConfig } from 'ng-ui-lib';

@Component({
  selector: 'app-login-form',
  standalone: true,
  imports: [FormGeneratorComponent, CommonModule],
  template: `
    <div>
      <h3>Login | Form Generator - custom config</h3>
      <lib-form-generator [formConfig]="config" (action)="onSubmit($event)"></lib-form-generator>
    </div>
  `,
})
export class LoginFormComponentTsComponent {
  config: FieldConfig[] = [
    createFieldConfig('username', FieldTypes.input, 'username', '', 'username', '', [
      { name: 'required', message: 'wymagane' },
    ]),
    createFieldConfig('password', FieldTypes.input, 'password', '', 'password', '', [
      { name: 'required', message: 'wymagane' },
    ]),
    createFieldConfig('agreement', FieldTypes.toggle, 'agreement', true),
    createFieldConfig('username', FieldTypes.button, 'send')
  ];

  onSubmit($event: any) {
    console.log($event);
  }
}
