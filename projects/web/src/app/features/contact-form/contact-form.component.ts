import { HttpClient } from '@angular/common/http';
import { Component, Input, computed, inject, input } from '@angular/core';
import { Observable, map } from 'rxjs';
import { CommonModule } from '@angular/common';
import { FormGeneratorComponent, FormValue } from 'ng-ui-lib';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'app-contact-form',
  standalone: true,
  imports: [CommonModule, FormGeneratorComponent],
  template: `
    <div>
      <h3>Form Generator</h3>
      <lib-form-generator (action)="onSubmit($event)" [formConfig]="data() | async"></lib-form-generator>
    </div>
  `,
})
export class ContactFormComponent {
  http = inject(HttpClient);
  url = input<string>();
  data = computed(() => this.http.get<any>(this.url()!).pipe(map(({ data }) => data)));

  onSubmit(value: FormValue) {
    console.log(value);
  }
}
