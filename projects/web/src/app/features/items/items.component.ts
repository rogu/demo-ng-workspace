import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import { DataGridModule } from 'ng-ui-lib';
import { Observable, map } from 'rxjs';
import { Api } from '../../utils/api';
import { MatButtonModule } from '@angular/material/button';
import { PassTemplateDirective } from 'projects/web/src/app/utils/pass-template.directive';

@Component({
  selector: 'app-items',
  standalone: true,
  imports: [DataGridModule, CommonModule, MatButtonModule, PassTemplateDirective],
  template: `
    <div class="px-4 pb-6">
      <h3 class="mb-4">ITEMS | DataGrid example</h3>
      <lib-data-grid [data]="dGridData$ | async" [headers]="['Nr.','Title', 'Price', 'Image', 'Action']">
        <tr *passTemplate="let item; let nr='nr'">
          <td>{{nr}}</td>
          <td>{{ item.title }}</td>
          <td>{{ item.price }}</td>
          <td><img [src]="item.imgSrc" width="30" alt="image" /></td>
          <td><button mat-raised-button (click)="more(item)">more</button></td>
        </tr>
      </lib-data-grid>
    </div>
  `,
  styles: ``
})
export class ItemsComponent {
  http = inject(HttpClient);
  dGridData$: Observable<any[]> = this.http.get(Api.ITEMS_END_POINT).pipe(map((resp: any) => resp.data));
  more(item: any) {
    alert(JSON.stringify(item, null, 4));
  }
}
