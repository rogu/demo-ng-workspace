import { Component, effect, HostBinding, inject } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { DataGridConfigService, IMenu, MenuComponent } from 'ng-ui-lib';
import { useTheme } from 'projects/web/src/app/utils/use-theme';
import { MatCardModule } from '@angular/material/card';
import { ToolBarComponent } from '../toolbar/toolbar.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, MenuComponent, ToolBarComponent, MatCardModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent {
  menu: IMenu[] = [
    { path: 'pages/home', name: 'home' },
    { path: 'pages/items', name: 'items' },
    { path: 'pages/contact', name: 'contact' },
    { path: 'pages/auth/login', name: 'login' },
    { path: 'pages/auth/register', name: 'register' },
  ];
  @HostBinding('class') className = '';
  gridConfigService = inject(DataGridConfigService);
  theme = useTheme();

  constructor() {
    effect(() => {
      this.gridConfigService.bgColor = this.theme.isDark() ? '#555' : '#eaeaea';
      this.className = this.theme.isDark() ? 'darkMode dark' : '';
    });
  }
}
