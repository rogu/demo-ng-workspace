import { Component, model, OnInit } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatToolbarModule } from '@angular/material/toolbar';

@Component({
  selector: 'app-toolbar',
  standalone: true,
  imports: [MatToolbarModule, MatSlideToggleModule, ReactiveFormsModule],
  templateUrl: './toolbar.component.html'
})
export class ToolBarComponent implements OnInit {
  dark = model<boolean>();
  toggleDarkMode = new FormControl();

  ngOnInit(): void {
    this.toggleDarkMode.setValue(this.dark())
    this.toggleDarkMode.valueChanges.subscribe((darkMode: any) => this.dark.set(darkMode));
  }
}
