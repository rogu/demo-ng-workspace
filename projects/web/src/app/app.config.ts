import { ApplicationConfig, importProvidersFrom } from '@angular/core';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { provideAnimations } from '@angular/platform-browser/animations';
import { routes } from './app.routes';
import { DataGridModule } from 'ng-ui-lib';

export const appConfig: ApplicationConfig = {
  providers: [
    importProvidersFrom(DataGridModule.forRoot({ bgColor: '#ff0000' })),
    provideRouter(routes, withComponentInputBinding()),
    provideHttpClient(withInterceptorsFromDi()),
    provideAnimations(),
  ],
};
