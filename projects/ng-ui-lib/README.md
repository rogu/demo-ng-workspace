# Angular Reusable Components Lib

## Usage in your app

### import

`import { DataGridModule, FormModule } from 'debugger-ng-ui-lib';`
then add this modules (or components) to imports section

### usage data-grid

`<lib-data-grid [data]="{}[]" [headers]="string[]">
    <tr *passTemplate="let item; let nr='nr'">
        <td>{{item.name}}</td>
        <td>{{item.phone}}</td>
    </tr>
</lib-data-grid>`

### usage form-generator

`<lib-form-generator (action)="onSubmit($event)" [formConfig]="FieldConfig[]"></lib-form-generator>`
