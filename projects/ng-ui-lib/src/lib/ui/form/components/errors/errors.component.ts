import { Component, input } from '@angular/core';
import { ValidationErrors } from '@angular/forms';
import { FieldValidator } from '../../form.models';
import { KeyValuePipe } from '@angular/common';

@Component({
    selector: 'lib-errors',
    templateUrl: './errors.component.html',
    styleUrls: ['./errors.component.scss'],
    standalone: true,
    imports: [KeyValuePipe]
})
export class ErrorsComponent {
  data = input<{
    errors: ValidationErrors;
    validators: FieldValidator[] | undefined;
  }>();

  getMessage(key: string) {
    try {
      return this.data()!.validators?.find((validator) => key === validator.name.toLowerCase())?.message;
    } catch (error) {
      console.log(`missing validator ${key}`);
    }
  }
}
