import { Component, forwardRef, input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { FieldConfig } from '../../../form.models';


@Component({
  selector: 'lib-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RadioComponent),
      multi: true,
    }
  ],
  standalone: true,
  imports: []
})
export class RadioComponent implements ControlValueAccessor {

  value: any;
  onTouch!: () => void;
  onModelChange!: (v: any) => void;
  config = input<FieldConfig>();

  writeValue(value: any): void {
    this.value = value;
  }
  registerOnChange(fn: any): void {
    this.onModelChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }
  change(value: any) {
    this.onModelChange(value);
    this.writeValue(value);
  }
}
