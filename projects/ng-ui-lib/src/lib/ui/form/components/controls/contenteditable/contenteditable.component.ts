import { Component, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'lib-contenteditable',
  styleUrls: ['./contenteditable.component.scss',`../../../../../tailwind.scss`],
  templateUrl: './contenteditable.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CustomContentEditableComponent),
      multi: true,
    }
  ],
  standalone: true
})
export class CustomContentEditableComponent implements ControlValueAccessor {
  onTouch!: () => void;
  onModelChange!: (v: any) => void;
  value: any;

  writeValue(value: any): void {
    this.value = value;
  }
  registerOnChange(fn: any): void {
    this.onModelChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }
  change(value: string) {
    this.onModelChange(value);
  }
}
