import { NgStyle, NgTemplateOutlet } from '@angular/common';
import {
  Component,
  ContentChild,
  Inject,
  OnChanges,
  Optional,
  SimpleChanges,
  TemplateRef,
  input,
  output
} from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, ValidatorFn, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { FieldConfig, FieldValidator, FormEvents, FormValue } from '../../form.models';
import { FORM_BG_COLOR } from '../../form.token';
import { FieldGeneratorDirective } from '../../directives/field-generator.directive';

@Component({
  selector: 'lib-form-generator',
  templateUrl: './form-generator.component.html',
  styleUrls: [`./form-generator.component.scss`],
  exportAs: 'lib-form-generator',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    NgStyle,
    FieldGeneratorDirective,
    NgTemplateOutlet
  ],
})
export class FormGeneratorComponent implements OnChanges {
  formConfig = input<FieldConfig[] | null>();
  action = output<FormValue>();
  form: FormGroup = this.fb.group({});
  errors!: any[];
  @ContentChild(TemplateRef) tpl!: TemplateRef<any>;

  constructor(
    private fb: FormBuilder,
    @Optional() @Inject(FORM_BG_COLOR) public bgColor: string
  ) { }

  onSubmit() {
    this.form.markAllAsTouched();
    this.errors = Object.entries(this.form.controls)
      .filter(([, { errors }]) => errors)
      .map(([name, { errors }]) => ({ [name]: errors }));
    this.action.emit({
      type: FormEvents.submit,
      payload: this.form.value,
      errors: this.errors.length ? this.errors : null,
    });
  }

  createForm() {
    this.formConfig()
      ?.filter(({ type }) => type !== 'button')
      .forEach(({ name, value, validators }) => {
        this.form.controls[name]
          ? this.form.setControl(name, this.fb.control(value, this.getValidators(validators)))
          : this.form.addControl(name, this.fb.control(value, this.getValidators(validators)))
        this.form.controls[name].valueChanges
          .pipe(debounceTime(1000))
          .subscribe((value) =>
            this.action.emit({ type: FormEvents.update, payload: { [name]: value } }));
      });
  }

  getValidators(validators: FieldValidator[] | undefined): ValidatorFn[] | null {
    if (!validators) return null;
    return validators.map((validator: FieldValidator): ValidatorFn => {
      return validator.name in Validators
        ? validator.param
          ? (Validators[validator.name] as (v: any) => any)(validator.param)
          : Validators[validator.name]
        : null;
    });
  }

  ngOnChanges({ formConfig: { currentValue } }: SimpleChanges): void {
    currentValue && this.createForm();
  }
}
