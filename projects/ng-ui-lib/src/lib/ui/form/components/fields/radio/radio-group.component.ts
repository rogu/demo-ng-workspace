import { Component } from '@angular/core';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';
import { FieldConfig } from '../../../form.models';
import { ErrorsComponent } from '../../errors/errors.component';
import { MatError, MatFormField } from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';
import { RadioComponent } from '../../controls/radio/radio.component';

@Component({
  template: `
    <mat-form-field [formGroup]="form">
      <!-- todo
        remove input and
        https://material.angular.io/guide/creating-a-custom-form-field-control
        -->
      <input [hidden]="true" matInput [formControlName]="config.name" />
      <div class="label">{{ config.label }}</div>
      <lib-radio [config]="config" [formControlName]="config.name"></lib-radio>
      @if (form.get(config.name)?.errors; as errors) {
      <mat-error>
        <lib-errors [data]="{ errors: errors, validators: config.validators }"></lib-errors>
      </mat-error>
      }
    </mat-form-field>
  `,
  standalone: true,
  imports: [ReactiveFormsModule, MatFormField, MatInput, RadioComponent, MatError, ErrorsComponent],
})
export class RadioGroupComponent {
  config!: FieldConfig;
  form!: FormGroup;
}
