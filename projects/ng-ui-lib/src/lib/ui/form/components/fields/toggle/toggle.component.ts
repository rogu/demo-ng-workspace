import { Component } from '@angular/core';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatError, MatFormField, MatLabel } from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';
import { FieldConfig } from '../../../form.models';
import { ErrorsComponent } from '../../errors/errors.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

@Component({
  template: `
    <div [formGroup]="form">
      <mat-slide-toggle [formControlName]="config.name">
        <mat-label>{{ config.label }}</mat-label>
      </mat-slide-toggle>
      @if (form.get(config.name)?.errors; as errors) {
      <mat-error>
        <lib-errors [data]="{ errors: errors, validators: config.validators }"></lib-errors>
      </mat-error>
      }
    </div>
  `,
  standalone: true,
  imports: [ReactiveFormsModule, MatLabel, MatSlideToggleModule, MatError, ErrorsComponent],
})
export class FormToggleComponent {
  config!: FieldConfig;
  form!: FormGroup;
}
