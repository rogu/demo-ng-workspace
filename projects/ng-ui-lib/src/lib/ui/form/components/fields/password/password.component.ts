import { Component } from '@angular/core';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';
import { FieldConfig } from '../../../form.models';
import { ErrorsComponent } from '../../errors/errors.component';
import { MatError, MatFormField, MatLabel } from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';

@Component({
    selector: 'lib-password',
    templateUrl: './password.component.html',
    standalone: true,
    imports: [ReactiveFormsModule, MatFormField, MatLabel, MatInput, MatError, ErrorsComponent]
})
export class PasswordComponent {
  config!: FieldConfig;
  form!: FormGroup;
}
