import { Component } from '@angular/core';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatOption } from '@angular/material/core';
import { FieldConfig } from '../../../form.models';
import { ErrorsComponent } from '../../errors/errors.component';
import { MatError, MatFormField, MatLabel } from '@angular/material/form-field';
import { MatSelect } from '@angular/material/select';

@Component({
  templateUrl: './form-select.component.html',
  standalone: true,
  imports: [ReactiveFormsModule, MatFormField, MatLabel, MatSelect, MatOption, MatError, ErrorsComponent]
})
export class FormSelectComponent {
  config!: FieldConfig;
  form!: FormGroup;
}
