import { Component } from '@angular/core';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';
import { FieldConfig } from '../../../form.models';
import { CustomContentEditableComponent } from '../../controls/contenteditable/contenteditable.component';
import { ErrorsComponent } from '../../errors/errors.component';

@Component({
  template: `
  <div [formGroup]="form">
    <label for="" class='label'>{{config.label}}</label>
    <lib-contenteditable class="{{config.cssClass}}" [formControlName]="config.name"></lib-contenteditable>
    @if (form.get(config.name)?.errors; as errors) {
      <lib-errors [data]="{errors: errors, validators: config.validators}">
      </lib-errors>
    }
  </div>
  `,
  standalone: true,
  imports: [ReactiveFormsModule, CustomContentEditableComponent, ErrorsComponent]
})
export class FormEditableComponent {
  config!: FieldConfig;
  form!: FormGroup;
}
