import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { FieldConfig } from '../../../form.models';

@Component({
    selector: 'lib-form-button',
    templateUrl: './form-button.component.html',
    standalone: true,
    imports:[MatButtonModule]
})
export class FormButtonComponent {
  config!: FieldConfig;
  form!: FormGroup;
}
