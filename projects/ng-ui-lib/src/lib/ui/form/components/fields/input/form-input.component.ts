import { Component } from '@angular/core';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatError, MatFormField, MatLabel } from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';
import { FieldConfig } from '../../../form.models';
import { ErrorsComponent } from '../../errors/errors.component';

@Component({
  templateUrl: './form-input.component.html',
  standalone: true,
  imports: [ReactiveFormsModule, MatFormField, MatLabel, MatInput, MatError, ErrorsComponent]
})
export class FormInputComponent {
  config!: FieldConfig;
  form!: FormGroup;
}
