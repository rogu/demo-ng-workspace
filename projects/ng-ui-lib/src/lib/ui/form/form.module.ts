import { MaterialModule } from '../../material.module';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { FieldGeneratorDirective } from './directives/field-generator.directive';
import { FormGeneratorComponent, FormInputComponent, FormSelectComponent, CustomContentEditableComponent, FormEditableComponent, FormTextareaComponent, ErrorsComponent, FormButtonComponent, PasswordComponent, RadioComponent, RadioGroupComponent } from './components';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        MaterialModule,
        FormGeneratorComponent,
        FormInputComponent,
        FormSelectComponent,
        FieldGeneratorDirective,
        CustomContentEditableComponent,
        FormEditableComponent,
        FormTextareaComponent,
        ErrorsComponent,
        FormButtonComponent,
        PasswordComponent,
        RadioComponent,
        RadioGroupComponent
    ],
    exports: [
        FormGeneratorComponent,
        FieldGeneratorDirective
    ]
})
export class FormModule { }
