import { InjectionToken, signal, WritableSignal } from '@angular/core';

export const FORM_BG_COLOR = new InjectionToken<WritableSignal<string>>('', {
  factory() {
    return signal('whitesmoke')
  }
});
