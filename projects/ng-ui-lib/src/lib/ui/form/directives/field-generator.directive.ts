import {
  ComponentRef,
  Directive,
  ViewContainerRef,
  input,
  inject,
  effect,
} from '@angular/core';
import {
  FormButtonComponent,
  FormEditableComponent,
  FormGeneratorComponent,
  FormInputComponent,
  FormSelectComponent,
  FormTextareaComponent,
  PasswordComponent,
  RadioGroupComponent,
} from '../components';
import { FieldConfig, FieldTypes } from '../form.models';
import { FormToggleComponent } from '../components/fields/toggle/toggle.component';

type Components =
  | FormInputComponent
  | FormSelectComponent
  | FormTextareaComponent
  | FormEditableComponent;

const fields: { [key in FieldTypes]: any } = {
  input: FormInputComponent,
  password: PasswordComponent,
  textarea: FormTextareaComponent,
  contenteditable: FormEditableComponent,
  select: FormSelectComponent,
  button: FormButtonComponent,
  radio: RadioGroupComponent,
  toggle: FormToggleComponent
};

@Directive({
  selector: '[libFieldGenerator]',
  standalone: true,
})
export class FieldGeneratorDirective {
  libFieldGenerator = input<FieldConfig>();
  container = inject(ViewContainerRef);
  formGeneratorComponent = inject(FormGeneratorComponent);

  constructor() {
    effect(() => {
      this.container.clear();
      const fieldType = this.libFieldGenerator()!.type;
      try {
        const component: ComponentRef<unknown> = this.container.createComponent(
          fields[fieldType]
        );
        const componentInstance = <Components>component.instance;
        componentInstance.config = this.libFieldGenerator()!;
        componentInstance.form = this.formGeneratorComponent.form;
      } catch (error) {
        console.warn(`component ${fieldType} not exists`);
      }
    });
  }
}
