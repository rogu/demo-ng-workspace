import {
  NgStyle,
  NgTemplateOutlet,
  SlicePipe,
  UpperCasePipe,
} from '@angular/common';
import {
  Component,
  ContentChild,
  TemplateRef,
  inject,
  input,
} from '@angular/core';
import { DataGridConfigService } from '../../data-grid.config.service';

@Component({
  selector: 'lib-data-grid',
  template: `
    @if (!data()?.length) {
    <div>you should pass data() any[]</div>
    } @if (!tpl) {
    <div>you should pass ng-template</div>
    }
    <div>
      @if (data()?.length) {
      <table class="my-table" [style.background]="configService.bgColor">
        <thead>
          <tr>
            @for (head of headers(); track head) {
            <th>
              {{ head }}
            </th>
            }
          </tr>
        </thead>
        <tbody>
          @for (row of data() | slice:currentPage * pageSize:currentPage *
          pageSize + pageSize; track row; let idx = $index) {
          <ng-container
            [ngTemplateOutlet]="tpl"
            [ngTemplateOutletContext]="{ $implicit: row, nr: idx + 1 }"
          ></ng-container>
          }
        </tbody>
      </table>
      }
    </div>
  `,
  styleUrls: ['./data-grid.component.scss'],
  standalone: true,
  imports: [NgStyle, NgTemplateOutlet, SlicePipe, UpperCasePipe],
})
export class DataGridComponent {
  configService = inject(DataGridConfigService);
  data = input<any[] | null>();
  headers = input<string[]>();
  @ContentChild(TemplateRef) tpl!: TemplateRef<any>;
  public pageSize = 5;
  public currentPage = 0;
  handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
  }
}
