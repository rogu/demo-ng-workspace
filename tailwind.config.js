/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: "class",

  content: ["./projects/**/src/**/*.{html,ts}"],
  borderWidth: {
    DEFAULT: "10px",
  },
  theme: {
    extend: {},
  },
  plugins: [],
};
