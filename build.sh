# eg. x-publish xx true dir

if [ "$1" ]; then
    npm run readme
    npx tailwindcss -m -o ./projects/ng-ui-lib/src/lib/tailwind.scss
    cd projects/ng-ui-lib
    npm version minor
    cd ../..
    ng build ng-ui-lib --configuration production
    cd dist/ng-ui-lib
    npm publish
    cd ../..
    git add .
    #git tag v$2
    git commit -m "$1"
    npm version minor
    git push origin main
    #git push --tags origin
else
    echo 'commit name missing!'
fi
